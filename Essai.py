"""
message=input("Bienvenue, apprenez à vous connaître ! ")

# signe astrologique

message=input("Quel est votre signe astrologique ? Appuyez sur Entrée")
day = int(input("Input birthday: "))
month = input("Input month of birth (e.g. march, july etc): ")
if month == 'december':
	astro_sign = 'Sagittarius' if (day < 22) else 'capricorn'
elif month == 'january':
	astro_sign = 'Capricorn' if (day < 20) else 'aquarius'
elif month == 'february':
	astro_sign = 'Aquarius' if (day < 19) else 'pisces'
elif month == 'march':
	astro_sign = 'Pisces' if (day < 21) else 'aries'
elif month == 'april':
	astro_sign = 'Aries' if (day < 20) else 'taurus'
elif month == 'may':
	astro_sign = 'Taurus' if (day < 21) else 'gemini'
elif month == 'june':
	astro_sign = 'Gemini' if (day < 21) else 'cancer'
elif month == 'july':
	astro_sign = 'Cancer' if (day < 23) else 'leo'
elif month == 'august':
	astro_sign = 'Leo' if (day < 23) else 'virgo'
elif month == 'september':
	astro_sign = 'Virgo' if (day < 23) else 'libra'
elif month == 'october':
	astro_sign = 'Libra' if (day < 23) else 'scorpio'
elif month == 'november':
	astro_sign = 'scorpio' if (day < 22) else 'sagittarius'
print("Your Astrological sign is :",astro_sign)

# Calcul IMC et état du poids

message=input("Quel est l'état de votre poids selon votre IMC ? Appuyez sur Entrée")
poids=float(input("poids en kg :"))
taille=float(input("taille en m :"))
imc=poids/taille**2
print("imc=",imc)
if imc<18:
        print ("Poids maigre")
elif imc<25:
        print ("Poids normal")
elif imc>25 and imc<40:
        print ("En surpoids")
elif imc>40:
        print ("En obésité")

import requests
import pandas as pd
from bs4 import BeautifulSoup
url = 'https://www.boulanger.com/c/micro-ondes'
uagent = ('Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like '
'Gecko) Chrome/24.0.1312.27 Safari/537.17')
headers = {"User-Agent": uagent}
resp = requests.get(url, headers=headers)
html_parser = BeautifulSoup(resp.content, "html.parser")
results = []
products = html_parser.find_all(class_="product-item__row")
for product in products:
    title = " ".join(product.find(class_="product-item__label").stripped_strings)
    price = product.find(class_="price__amount").text.strip()
    results.append({"title": title, "price": price})
    
df = pd.DataFrame(results)
xlsx_file = "out.xlsx"
df.to_excel(xlsx_file, index=False)
print(f'{df.shape} df saved as {xlsx_file}')


# Création du jeu SNAKE
import pygame
import random
# Initialisation du jeu : 
pygame.init()
font = pygame.font.SysFont('Arial', 30)

# Les couleurs : 
blue = (0, 0, 255) # hex code for blue
black = (0, 0, 0) # hex code for black
red = (255, 0, 0) # hex code for red
screen_width = 720
screen_height = 480
screen = pygame.display.set_mode((screen_width, screen_height))

# Positionner le serpent au milieu de l'écran
snake_x = screen_width / 2
snake_y = screen_height / 2
snake_speed = 30
snake_size = 10
snake_length = 1
snake_blocks = []

fruit_x = 300
fruit_y = 400

speed_x = 0
speed_y = 10

game_over = False

running = True
clock = pygame.time.Clock()

# Le serpent bouge : 
while running:
  # Si le joueur n'a pas perdu le jeu :
  if not game_over:
    screen.fill((255,255,255)) # 255, 255, 255 is hexadecimal for the colour black

    # Garder le serpent qui attrape le bloc dans la bonne position 
    snake_head = []
    snake_head.append(snake_x)
    snake_head.append(snake_y)
    snake_blocks.append(snake_head)

    # La taille du serpent est adéquate
    if len(snake_blocks) > snake_length:
      del snake_blocks[0]

    # Vérifier la bonne place des blocs par rapport au serpent
    for x in snake_blocks[:-1]:
      if x == snake_head:
        game_over = True

    # Dessiner un bloc
    for block in snake_blocks:
      pygame.draw.rect(screen, blue, [block[0], block[1], snake_size, snake_size])
    pygame.draw.rect(screen, red, [fruit_x, fruit_y, snake_size, snake_size])

    # Mettre à jour la vitesse du serpent 
    snake_x += speed_x
    snake_y += speed_y

    # Si le serpent mange le fruit, faire apparaitre le nouveau fruit et mettre à jour la taille du serpent
    if snake_x == fruit_x and snake_y == fruit_y:
      fruit_x = round(random.randrange(0, screen_width - snake_size) / 10.0) * 10.0
      fruit_y = round(random.randrange(0, screen_height - snake_size) / 10.0) * 10.0
      snake_length += 1

    # Si le serpent touche la gauche ou la droite de l'ecran
    if (snake_x >= screen_width or snake_x < 0 or
      # Si le serpent touche la paroi du haut ou du bas de l'ecran
      snake_y >= screen_height or snake_y < 0):
        # Game over
        game_over = True

  # Descriptif du game over (score etc.)
  else:
    screen.fill(blue)
    score = font.render('You scored ' + str(snake_length), False, black)
    screen.blit(score, (10, screen_height / 2 - 100))
    text = font.render('You lost! Press \'Q\' to quit, or Spacebar to play again', False, black)
    screen.blit(text, (10, screen_height / 2))

  # Mise a jour de l'écran : 
  pygame.display.flip()
  clock.tick(snake_speed)

  # Manipulations du joueur
  for event in pygame.event.get():
    # Si sélection "Keydown"
    if event.type == pygame.KEYDOWN:
        # Si bouton "Q", quitter le jeu :
        if event.key == pygame.K_q:
            running = False
        # Si barre espace, redemarrer le jeu : 
        if event.key == pygame.K_SPACE:
            game_over = False
            snake_x = screen_width / 2
            snake_y = screen_height / 2
            snake_blocks = []
            snake_length = 1
        # Mouvements en fonction des flèches du clavier : 
        if event.key == pygame.K_UP:
            speed_x = 0
            speed_y = -10
        if event.key == pygame.K_DOWN:
            speed_x = 0
            speed_y = 10
        if event.key == pygame.K_LEFT:
            speed_y = 0
            speed_x = -10
        if event.key == pygame.K_RIGHT:
            speed_y = 0
            speed_x = 10
    # Le joueur sélectionne "quitter"
    if event.type == pygame.QUIT:
      # Si false, arrêter 
      running = False


# Faire un nuage de mots classique

from wordcloud import WordCloud
import matplotlib.pyplot as plt

text = 'La promotion 21 de cette année a rencontré les méthodologies de audit informatique. Audit Audit Audit Conseil SIEE SIEE SIEE SIEE SIEE SIEE SIEE SIEE SIEE mission mission mission mission mission méthodologie méthodologie méthodologie méthodologie méthodologie certifier certifier certifier certifier mission conseil informatique projet projet projet projet projet IT IT IT IT IT IT IT IT IT IT IT IT IT IT En audit IT, la méthodologie de base reste la même mais le type de mission et les méthodes des différentes entreprises peuvent faire varier la méthode d''audit. L''audit est dissocié du conseil : le conseil peut accompagner le client dans une implémentation ou un nouveau projet, tandis que l''audit fait sert en parti à certifier les comptes ou à diagnostiquer l''état informatique de l''entreprise pour servir les CAC ou dans le cadre d''opérations opérations opérations opérations opérations M&A, par exemple.' 
exclure_mots = ['reste','de','le','ou','différentes','le','le','en','varier','peut','daudit','une','que','est','peuvent','sert','différentes','dans','pour','cette','cette','nouveau','letat','du','rencontré','à','le','de','le','de''tandis','et','exemple','fait','de','la','le','ou','dans','mais','le','de','la','le','la','la','la','de','la','le']

# Generate a word cloud image
wordcloud = WordCloud().generate(text)

plt.imshow(wordcloud, interpolation="bilinear")
plt.axis('off')
plt.show()


# Données sous graphique à points

import pandas as pd
import matplotlib.pyplot as plt
   
data = {'Prix_Du_Plat': [12,13,14,15,16,17,18,19],
        'Nombre_Commandes': [7,8,9,7,8,9,7,6]
       }
  
df = pd.DataFrame(data,columns=['Prix_Du_Plat','Nombre_Commandes'])
df.plot(x ='Prix_Du_Plat', y='Nombre_Commandes', kind = 'scatter')
plt.show()

# Données sous graphique en courbe

import pandas as pd
import matplotlib.pyplot as plt
   
data = {'Pays': ['France','Angleterre','Espagne','Allemagne','Italie'],
        'Nombre_Habitants_2020': [67000000,67000000,47000000,83000000,60000000]
       }
  
df = pd.DataFrame(data,columns=['Pays','Nombre_Habitants_2020'])
df.plot(x ='Pays', y='Nombre_Habitants_2020', kind = 'bar')
plt.show()

# Données sous graphique camembert

import pandas as pd
import matplotlib.pyplot as plt

data = {'Réponses': [300,500,700]}
df = pd.DataFrame(data,columns=['Réponses'],index = ['Oui','Non','Peut-être'])

df.plot.pie(y='Réponses',figsize=(5, 5),autopct='%1.1f%%', startangle=90)
plt.show()

import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-5,5,100)

# Function
y = x**2 + 2*x + 1

# Axes 
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

# Graphique de la fonction
plt.plot(x,y, 'r')
plt.grid(True, axis = 'y')
plt.legend(['Représentation graphique'], loc = 4, shadow = True)
plt.title("Equation pour l''exercice sous GIT", fontsize = 18)

plt.show()

# Création d'un scatterplot

import matplotlib.pyplot as plt

x = [20, 6, 19, 1, 25, 13, 7, 28, 13, 3, 8, 7]
y = [100, 87, 36, 98, 13, 20, 32, 46, 74, 38, 92, 19]

plt.scatter(x, y)
plt.show()


# Graphique visualisation 3D

from pylab import *
from mpl_toolkits.mplot3d import Axes3D
ax = Axes3D(figure())
X = np.arange(-6, 6, 0.5)
Y = np.arange(-6, 6, 0.5)
X, Y = np.meshgrid(X, Y)
R = np.sqrt(X**2 + Y**2)
Z = np.sin(R)
ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='cool')
ax.plot_surface(X, Y, R, rstride=1, cstride=1, cmap='hot')
show()
"""

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm

dx, dy = 0.015, 0.05
x = np.arange(-4.0, 4.0, dx)
y = np.arange(-4.0, 4.0, dy)
X, Y = np.meshgrid(x, y)
extent = np.min(x), np.max(x), np.min(y), np.max(y)
z1 = np.add.outer(range(8), range(8)) % 2
plt.imshow(z1, cmap="binary_r", interpolation="nearest", extent=extent, alpha=1)

def chess(x, y):
    return (1 - x / 2 + x ** 5 + y ** 6) * np.exp(-(x ** 2 + y ** 2))
z2 = chess(X, Y)
plt.imshow(z2, alpha=0.7, interpolation="bilinear", extent=extent)
plt.title("Chess Board with Python")
plt.show()

